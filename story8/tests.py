from django.test import TestCase, LiveServerTestCase
from selenium.webdriver.chrome import webdriver
from selenium import webdriver
# Create your tests here.
class ProfileFunctionalTest(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(ProfileFunctionalTest, cls).setUpClass()
        cls.browser = webdriver.Chrome('./chromedriver')

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(ProfileFunctionalTest, cls).tearDownClass()

    # def setUp(self):
    #     self.browser = webdriver.Chrome('./chromedriver')
    #     self.browser.implicitly_wait(10)
    #     super(StatusFunctionalTest, self).setUp()


    # def tearDown(self):
    #     self.browser.quit()
    #     super(StatusFunctionalTest, self).tearDown()

    def test_accordion_content(self):
        broswer = self.browser
        self.browser.get('%s%s' % (self.live_server_url, '/profile/'))
        name_input = self.browser.find_element_by_id('about')
        name_input.click()
        assert "My name" in self.browser.page_source
        name_input = self.browser.find_element_by_id('activity')
        name_input.click()
        assert "HEAD OF PUBLIC RELATION" in self.browser.page_source
        name_input = self.browser.find_element_by_id('experience')
        name_input.click()
        assert "PROJECT OFFICER / CHIEF OFFICER" in self.browser.page_source
        name_input = self.browser.find_element_by_id('achievement')
        name_input.click()
        assert "ENGLISH PROFICIENCY TEST, College Board Scholastic Assessment Test (SAT)" in self.browser.page_source
        # status_input = self.browser.find_element_by_id('status')
        # status_input.send_keys('Status')
        # self.browser.find_element_by_class_name('btn').submit()
        # assert "Apakah" in self.browser.page_source
        # self.browser.find_element_by_id('ya').submit()
        # assert "Nama" in self.browser.page_source
        # self.browser.find_element_by_id('')
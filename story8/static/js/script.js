$(document).ready(function() {
    $(".set > a").on("click", function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this)
          .siblings(".content")
          .slideUp(200);
        $(".set > a i")
          .removeClass("fa-chevron-down")
          .addClass("fa-chevron-up");
      } else {
        $(".set > a i")
          .removeClass("fa-chevron-down")
          .addClass("fa-chevron-up");
        $(this)
          .find("i")
          .removeClass("fa-chevron-up")
          .addClass("fa-chevron-down");
        $(".set > a").removeClass("active");
        $(this).addClass("active");
        $(".content").slideUp(200);
        $(this)
          .siblings(".content")
          .slideDown(200);
      }
    });
  });
  
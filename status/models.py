from django.db import models

# Create your models here.

class StatusModel(models.Model):
    nama = models.CharField(max_length=512)
    status = models.CharField(max_length=1024)

    def __str__(self):
        return self.nama
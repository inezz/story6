from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import *
from .forms import *
# Create your views here.


def status(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid:
            return render(request, 'confirm.html', {'form': form})
    else:
        status = StatusModel.objects.all().values()
        form = StatusForm(auto_id=True)
        return render(request, 'status.html', {'form': form, 'status': status})


def submit(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('status:status')
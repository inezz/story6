from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.http import HttpRequest
from .models import StatusModel
from .forms import StatusForm
from selenium.webdriver.chrome import webdriver
from selenium import webdriver
# from selenium import webdriver
from django.apps import apps
from .apps import StatusConfig
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class StatusUnitTest(TestCase):
    
    def test_page_exist(self):
        page = Client().get('/status/')
        self.assertEqual(page.status_code, 200)

    def test_right_template(self):
        page = Client().get('/status/')
        self.assertTemplateUsed(page, 'status.html')

    def test_create_status(self):
        new_status = StatusModel.objects.create(nama="nama", status = 'coba pertama')
        self.assertTrue(isinstance(new_status, StatusModel))
        self.assertTrue(new_status.nama, "nama")
        available_status = StatusModel.objects.all().count()
        self.assertEqual(available_status,1)
        self.assertEqual(str(new_status), "nama")

    def test_form_validation_for_filled_items(self) :
        response = self.client.post('/status', data={'nama': 'nama','status' : 'Status'})
        response_content = response.content.decode()
        self.assertIn(response_content, 'Status')


class StatusFunctionalTest(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(StatusFunctionalTest, cls).setUpClass()
        cls.browser = webdriver.Chrome('./chromedriver')

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(StatusFunctionalTest, cls).tearDownClass()

    # def setUp(self):
    #     self.browser = webdriver.Chrome('./chromedriver')
    #     self.browser.implicitly_wait(10)
    #     super(StatusFunctionalTest, self).setUp()


    # def tearDown(self):
    #     self.browser.quit()
    #     super(StatusFunctionalTest, self).tearDown()

    def test_send_status(self):
        broswer = self.browser
        self.browser.get('%s%s' % (self.live_server_url, '/status/'))
        name_input = self.browser.find_element_by_id('nama')
        name_input.send_keys('Nama')
        status_input = self.browser.find_element_by_id('status')
        status_input.send_keys('Status')
        self.browser.find_element_by_class_name('btn').submit()
        assert "Apakah" in self.browser.page_source
        self.browser.find_element_by_id('ya').submit()
        assert "Nama" in self.browser.page_source
        # self.browser.find_element_by_id('')
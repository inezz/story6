from django import forms
from .models import StatusModel
from django.utils.translation import gettext_lazy as _

class StatusForm(forms.ModelForm):
    
    class Meta:
        model = StatusModel
        fields = '__all__'
        widgets = {
            'nama': forms.TextInput(),
            'status': forms.TextInput(),
        }
        labels = {
            'nama': _('Nama'),
            'status': _('Status'),
        }
